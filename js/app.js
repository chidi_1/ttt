$('document').ready(function () {

    if (device.tablet() || device.mobile() || $(window).width() < 1023) {
        set_height();

        $(window).resize( set_height());

        $('.mobile-slider').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: false
        });

        setTimeout(function () {
            $('.promo__slider_img').animate({'opacity': 1}, 500)
        }, 1500)
    }

    $('.promo__slider_img').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        draggable: false,
        speed: 700,
        fade: false,
        asNavFor: '.promo__nav, .promo__slider_content'
    });

    $('.promo__slider_content').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        draggable: false,
        speed: 700,
        asNavFor: '.promo__nav, .promo__slider_img',
        adaptiveHeight: true
    });

    setTimeout(function () {
        $('.promo__slider_content .slick-active .promo__content').animate({'opacity': 1}, 300)
    }, 500)

    $('.promo__slider_content').on('afterChange', function(event, slick, currentSlide, nextSlide){
        $('.promo__slider_content .slick-active .promo__content').animate({'opacity': 1}, 200)
    });
    $('.promo__slider_content').on('beforeChange', function(event, slick, currentSlide, nextSlide){
        $('.promo__slider_content .slick-active .promo__content').animate({'opacity': 0}, 200)
    });


    $('.promo__nav').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        asNavFor: '.promo__slider_img, .promo__slider_content',
        dots: false,
        speed: 700,
        focusOnSelect: true,
        responsive: [
            {
                breakpoint: 1920,
                settings: {
                    slidesToShow: 5
                }
            },
            {
                breakpoint: 1440,
                settings: {
                    slidesToShow: 4
                }
            },
            {
                breakpoint: 1280,
                settings: {
                    slidesToShow: 3
                }
            }
        ]
    });

    $('.js--toggle-menu').on('click', function () {
        $('.header').toggleClass('open');
        return false;
    });

    function set_height() {
        let height = $(window).height();
        $('.promo').css('height', height);
        $('.header__menu').css('height', height);
    }

    $('.header__menu').on('click', function (e) {
        if($(e.target).hasClass('js--close-menu')){
            $('.header').toggleClass('open');
        }
    })
});